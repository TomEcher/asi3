package com.sp.mapper;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.dto.CardDTO;
import com.sp.dto.TemplateCardDTO;
import com.sp.dto.TransactionDTO;
import com.sp.dto.TransactionPostDTO;
import com.sp.dto.UserDTO;
import com.sp.model.Transaction;
@Component
public class TransactionMapper {

	@Autowired
	TransactionMapper tm;
	



	public TransactionDTO toDTO(Transaction transaction,CardDTO card){
		TransactionDTO trans = new TransactionDTO(transaction.getId(), transaction.getUserBuyer_id(), transaction.getUserSeller_id(), transaction.getCard_id(), transaction.getUpdateAt(), transaction.getStatus(), card);
		return trans;
		
	}
	
	public Transaction PosttoModel(TransactionPostDTO transaction){
		return new Transaction(null, transaction.getSeller_id(), transaction.getCard_id(),transaction.getUpdateAt(),transaction.getStatus());
		
	}
	

}