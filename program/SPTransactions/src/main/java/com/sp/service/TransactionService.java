package com.sp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sp.dto.CardDTO;
import com.sp.dto.CardS2SDTO;
import com.sp.dto.MessageResponseDTO;
import com.sp.dto.UserDTO;
import com.sp.dto.UserPutDTO;
import com.sp.model.Transaction;
import com.sp.repository.TransactionRepository;

@Service
public class TransactionService {
	@Autowired
	TransactionRepository tRepository;
	
	
	
    /**
    *
    * Création d'une carte
    *
    */	
	public void addCard(Transaction tc) {
		Transaction createdTransaction= tRepository.save(tc);
		System.out.println(createdTransaction);
	}
	
    /**
    *
    * Récupération d'une carte à partir d'un ID
    *
    */		
	public Transaction getTransaction(Long id) {
		Optional<Transaction> cOpt = tRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}

    /**
    *
    * Récupération de l'ensemble des cartes
    *
    */	
	public List<Transaction> getAllTransaction() {
		List<Transaction> transactions = tRepository.findAll();
		return transactions;
	}

    /**
    *
    * Récupération de l'ensemble des cartes en fonction d'un status
    *
    */		
	public List<Transaction> getAllTransactionActive(Long status) {
		List<Transaction> transactions = tRepository.findByStatus(status);
		return transactions;
	}

    /**
    *
    * Vérifie si la carte est déjà en vente
    *
    */	
	public Boolean ifCardOnSale(long card) {
		Optional<Transaction> cOpt = tRepository.findByStatusAndCardId((long) 0,card);
		if (cOpt.isPresent()) {
			return true;
		}else {
			return false;
		}
	}
	
    /**
    *
    * Vérifie si la transaction est toujours d'activité
    *
    */		
	public Boolean ifTransactionIsActive(Transaction transaction) {
		Optional<Transaction> cOpt = tRepository.findByIdAndStatus(transaction.getId(), (long) 0);
		if (cOpt.isPresent()) {
			return true;
		}else {
			return false;
		}
	}
	
	
	public void changeOwner(Long user_id, Long card_id) {
		RestTemplate restTemplate = new RestTemplate();
		CardS2SDTO card = new CardS2SDTO();
		card.setCardId(card_id);
		card.setUserId(user_id);
		try {
		    HttpEntity<CardS2SDTO> entity = new HttpEntity<CardS2SDTO>(card);
		    restTemplate.exchange("http://rproxy:8080/cards", HttpMethod.PUT, entity, CardS2SDTO.class);
		}catch(Exception e ){

			System.out.println("UPDATE ERROR");
        }
	}
	
	public void changeBalance(Long user_id, float balance) {
		RestTemplate restTemplate = new RestTemplate();
		UserPutDTO user = new UserPutDTO();
		user.setId_user(user_id);
		user.setBalance(balance);
		try {
		    HttpEntity<UserPutDTO> entity = new HttpEntity<UserPutDTO>(user);
		    restTemplate.exchange("http://rproxy:8080/users/", HttpMethod.PUT, entity, UserPutDTO.class);
		}catch(Exception e ){

			System.out.println("UPDATE ERROR");
        }
		
	}
	
	public CardDTO getCardbyID(Long card_id) {
		RestTemplate restTemplate = new RestTemplate();

			System.out.println("http://rproxy:8080/cards/"+card_id);
			CardDTO card = restTemplate.getForObject("http://rproxy:8080/cards/"+card_id, CardDTO.class);
			return card;

	}
	
}
