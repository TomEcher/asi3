package com.sp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Transactions")
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    private Long id;
	

	public Long userBuyer_id;
	public Long userSeller_id;
	public Long cardId;

	public Date updateAt;
	
	public Long status;

	public Transaction() {}

	public Transaction(Long userBuyer_id, Long userSeller_id, Long card_id, Date updateAt, Long status) {
		super();
		this.userBuyer_id = userBuyer_id;
		this.userSeller_id = userSeller_id;
		this.cardId = card_id;
		this.updateAt = updateAt;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserBuyer_id() {
		return userBuyer_id;
	}

	public void setUserBuyer_id(Long userBuyer_id) {
		this.userBuyer_id = userBuyer_id;
	}

	public Long getUserSeller_id() {
		return userSeller_id;
	}

	public void setUserSeller_id(Long userSeller_id) {
		this.userSeller_id = userSeller_id;
	}

	public Long getCard_id() {
		return cardId;
	}

	public void setCard_id(Long card_id) {
		this.cardId = card_id;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}
	
	
	
}
