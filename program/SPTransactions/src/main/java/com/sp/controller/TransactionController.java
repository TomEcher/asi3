package com.sp.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.sp.dto.CardDTO;
import com.sp.dto.MessageResponseDTO;
import com.sp.dto.TransactionDTO;
import com.sp.dto.TransactionPostDTO;
import com.sp.dto.UserDTO;
import com.sp.mapper.TransactionMapper;
import com.sp.model.Transaction;
import com.sp.service.TransactionService;
import com.sp.tools.UserLoginTools;


@RestController
@CrossOrigin(origins = "*")
public class TransactionController {
    @Autowired
    TransactionService tService;
    
    @Autowired
    TransactionMapper tMapper;

    @Autowired
    UserLoginTools ulTools;
    
    @Autowired
    HttpServletRequest request;
    /**
    *
    * R�cup�ration des informations d'une transaction par son ID
    *
    */	
	@RequestMapping(method=RequestMethod.GET,value="/transactions/{id}")
	public TransactionDTO getMsg(@PathVariable String id) {
		
        Transaction t= tService.getTransaction(Long.parseLong(id));
		CardDTO card = tService.getCardbyID(t.getCard_id());

        
		System.out.println("CARTE : "+ card);
		System.out.println("TRANSACTION : "+ t);
        return tMapper.toDTO(t, card);
	}
	
    /**
    *
    * R�cup�ration de l'ensemble des transactions
    *
    */	
	@RequestMapping(method=RequestMethod.GET,value="/transactions")
	public List<TransactionDTO> getMsg2(@RequestParam("status") Optional<String> status) {
		
		List<TransactionDTO> TransactionList = new ArrayList<TransactionDTO>();
		if (status.isPresent()) {
			String myStatus = status.get();
			List<Transaction> Transaction = tService.getAllTransactionActive(Long.parseLong(myStatus));
			for (int i = 0; i < Transaction.size(); i++) {
				CardDTO card = tService.getCardbyID(Transaction.get(i).getCard_id());

				TransactionList.add(tMapper.toDTO(Transaction.get(i), card));
			  }
		}else {
			List<Transaction> Transaction = tService.getAllTransaction();
			for (int i = 0; i < Transaction.size(); i++) {
				CardDTO card = tService.getCardbyID(Transaction.get(i).getCard_id());

				TransactionList.add(tMapper.toDTO(Transaction.get(i),card));
			  }
		}
		return TransactionList;
	}

    /**
    *
    * Cr�ation d'une transaction
    *
    */	
	
	@RequestMapping(method=RequestMethod.POST,value="/transactions")
	public ResponseEntity<MessageResponseDTO> addUser(@RequestBody TransactionPostDTO transactionPostDTO, Principal principal) {
		UserDTO u = ulTools.getUserByToken(request.getHeader("Authorization"));
		transactionPostDTO.setSeller_id(u.getId());
		Transaction transaction = tMapper.PosttoModel(transactionPostDTO);
		if(tService.ifCardOnSale(transaction.getCard_id())) {
			return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "transaction creation failed"), HttpStatus.CONFLICT);
 
		}
		if(transaction.getUserSeller_id() != null && transaction.getCard_id() != null) {
			tService.addCard(transaction);
			return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, "successful transaction creation"), HttpStatus.ACCEPTED);
		}else {
			return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "transaction creation failed : some arguments are wrong"), HttpStatus.CONFLICT);

		}
		
	}
	
	/**
    *
    * Modification des informations de la transaction via le PUT
    *
    */	
	@RequestMapping(method=RequestMethod.PUT,value="/transactions/{id}")
	public ResponseEntity<MessageResponseDTO> addUser(@PathVariable String id, Principal principal) {
		UserDTO u = ulTools.getUserByToken(request.getHeader("Authorization"));
        try {
        	
            Transaction transaction = tService.getTransaction(Long.parseLong(id));

    		UserDTO us = ulTools.getUserByToken(request.getHeader("Authorization"));
            System.out.println(tService.ifTransactionIsActive(transaction));
            CardDTO card = tService.getCardbyID(transaction.getCard_id());
            System.out.println(card);

    		if(tService.ifTransactionIsActive(transaction)) {
    			if(u.getBalance() >= card.getTc().getPrice()) {
    			transaction.setUserBuyer_id(u.getId());
    			transaction.setStatus((long) 1);
    			transaction.setUpdateAt(new Date());
                System.out.println(transaction);

    			// APPEL USER GIVE AND REMOVE MONEY
                tService.changeBalance(u.getId(), - card.getTc().getPrice());
                tService.changeBalance(transaction.getUserSeller_id(), card.getTc().getPrice());
    			// APPEL USER CHANGE OWNER
    			tService.changeOwner(transaction.getUserBuyer_id(), transaction.getCard_id());
    			
    			
    			
    			tService.addCard(transaction);
    			}else {
            		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "transaction update failed : no enought money"), HttpStatus.CONFLICT);

    			}
    		}else{
        		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "transaction update failed"), HttpStatus.CONFLICT);
    		}
        	
        }catch(Exception e ){
            System.out.println("Probl�me lors de l'achat");
    		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "transaction update failed"), HttpStatus.CONFLICT);

        }
		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, "successful transaction update"), HttpStatus.ACCEPTED);

	}
	
	
	

}
