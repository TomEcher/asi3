package com.sp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpAppTransactions {
    
    public static void main(String[] args) {
        SpringApplication.run(SpAppTransactions.class,args);
    }
}