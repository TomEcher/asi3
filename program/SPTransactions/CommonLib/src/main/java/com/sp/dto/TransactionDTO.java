package com.sp.dto;

import java.util.Date;

import javax.persistence.ManyToOne;


public class TransactionDTO {
    private Long id;
	public Long userBuyer_id;
	public Long userSeller_id;
	public Long card_id;
	public Date updateAt;
	public Long status;
	public CardDTO card;
	
	
	
	public TransactionDTO(Long id, Long userBuyer_id, Long userSeller_id, Long card_id, Date updateAt, Long status, CardDTO card) {
		super();
		this.id = id;
		this.userBuyer_id = userBuyer_id;
		this.userSeller_id = userSeller_id;
		this.card_id = card_id;
		this.updateAt = updateAt;
		this.status = status;
		this.card = card;
	}
	public CardDTO getCard() {
		return card;
	}
	public void setCard(CardDTO card) {
		this.card = card;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserBuyer_id() {
		return userBuyer_id;
	}
	public void setUserBuyer_id(Long userBuyer_id) {
		this.userBuyer_id = userBuyer_id;
	}
	public Long getUserSeller_id() {
		return userSeller_id;
	}
	public void setUserSeller_id(Long userSeller_id) {
		this.userSeller_id = userSeller_id;
	}
	public Long getCard_id() {
		return card_id;
	}
	public void setCard_id(Long card_id) {
		this.card_id = card_id;
	}
	public Date getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}


	
}
