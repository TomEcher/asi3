package com.sp.dto;

public class UserGetDTO {

	public Long id;
	public String lname;
	public String fname;
	public String pseudo;
	public Float balance;
	
	
	public UserGetDTO(Long id, String lname, String fname, String pseudo, Float balance) {
		super();
		this.id = id;
		this.lname = lname;
		this.fname = fname;
		this.pseudo = pseudo;
		this.balance = balance;
	}
	
	
	public UserGetDTO() {
	}

	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public Float getBalance() {
		return balance;
	}
	public void setBalance(Float balance) {
		this.balance = balance;
	}
	
	
	
}
