package com.sp.dto;


public class TokenDTO {

	public String token;

	public TokenDTO() {
	}	
	
	
	public TokenDTO(String token) {
		super();
		this.token = token;
	}


	public String getToken() {
		return token;
	}

	public void setMessage(String token) {
		this.token = token;
	}


	
}
