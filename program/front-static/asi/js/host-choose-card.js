function showCard(id){
    $.ajax({
        url: "http://"+window.location.host+":8080/cards/"+id,
        headers: {
            "Authorization": "Bearer " + localStorage.getItem('bearer')
        },
        type: 'GET',
        dataType: 'json',
        success: function(card) {
            document.querySelector("#chooseCard");
            let container_card= document.querySelector("#container_card");
            container_card.innerHTML = `
                <div class="ui special cards">
                    <div class="card">
                        <div class="content">
                            <div class="ui grid">
                                <div class="three column row">
                                    <div class="column">
                                        <h5>${card.tc.name}</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="image imageCard">
                                <div class="blurring dimmable image">
                                    <div class="ui fluid image">
                                        <a class="ui left corner label">
                                            ${card.tc.family}
                                        </a>
                                        <img id="cardImgId" class="ui centered image" src="${card.tc.avatarURL}">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="ui form tiny">
                                    <div class="field">
                                        <label id="cardNameId"></label>
                                        <textarea id="cardDescriptionId" class="overflowHiden" readonly="" rows="2">${card.tc.description}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <i class="heart outline icon"></i><span id="cardHPId"> HP ${card.tc.hp}</span> 
                                <div class="right floated ">
                                        <span id="cardEnergyId">Energy ${card.tc.energy}</span>
                                    <i class="lightning icon"></i>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <span class="right floated">
                                        <span id="cardAttackId"> Attack ${card.tc.attack}</span> 
                                    <i class=" wizard icon"></i>
                                </span>
                                <i class="protect icon"></i>
                                <span id="cardDefenceId">Defense ${card.tc.defense}</span> 
                            </div>
                            <a href="./createRoom.html?card_id=${card.id}">
                                <div class="ui bottom attached button" id="chooseCard">
                                    <i class="gavel icon"></i>
                                    Play with <span id="cardPriceId"> ${card.tc.name}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            `;
        },
        error: function(errorThrown){
            swal({
                title: 'Choose error',
                text: errorThrown.responseJSON.message,
                icon: 'error',
                timer: 1000,
                button:false
            })        
        }
    })
}