$.ajax({
    url: "http://"+window.location.host+":8080/cards/",
    headers: {
        "Authorization": "Bearer " + localStorage.getItem('bearer')
    },
    type: 'GET',
    dataType: 'json',
    success: function(cardList) {
        let template_cards = document.querySelector("#chooseCards");
        for(const card of cardList){
            let clone_cards = document.importNode(template_cards.content, true);
            newContent= clone_cards.firstElementChild.innerHTML
                        .replace(/{{id}}/g, card.id)
                        .replace(/{{family_name}}/g, card.tc.family)
                        .replace(/{{name}}/g, card.tc.name)
                        .replace(/{{hp}}/g, card.tc.hp)
                        .replace(/{{affinity}}/g, card.tc.affinity)
                        .replace(/{{energy}}/g, card.tc.energy)
                        .replace(/{{attack}}/g, card.tc.attack)
                        .replace(/{{defense}}/g, card.tc.defense)
                        .replace(/{{price}}/g, card.tc.price);
            clone_cards.firstElementChild.innerHTML= newContent;
        
            let container_cards= document.querySelector("#container_cards");
            container_cards.appendChild(clone_cards);
            
        }
        $( "#container_cards tr" ).on( "click", function( event ) {
            var cardID = $(this).find("td").eq(0).html();
            if (cardID) {
                showCard(cardID);
            }
        });
    },
    error: function(errorThrown){
        swal({
            title: 'Choose error',
            text: 'An error occured',
            icon: 'error',
            timer: 1000,
            button:false
        })        
    }
})
