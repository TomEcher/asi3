function send(event) {
    event.preventDefault()

    var user = {
        fname:$("#fname").val(),
        lname:$("#lname").val(),
        pseudo:$("#pseudo").val(),
        password:$("#password").val(),
    }
    if (user.password != $("#confirm_password").val()) {
        alert("Passwords do not match.");
        return ;
    }
    
    $.ajax({
        url: 'http://'+window.location.host+':8080/users',
        type: "POST",
        dataType: 'json',
        data: JSON.stringify(user),
        contentType: 'application/json;charset=UTF-8',
        success: function() {
            swal({
                title: 'Register Success',
                text: 'Redirecting...',
                icon: 'success',
                timer: 1000,
                button:false
            }).then(() => {
                window.location.href = "./login.html";
            })
        },
        error: function(errorThrown){
            if(errorThrown.responseJSON.status==0){
                sweetAlert("Oops...", errorThrown.responseJSON.message, "error");
            }
            else{
                sweetAlert("Oops...", "An error occured", "error");
            }
        }
    })
}