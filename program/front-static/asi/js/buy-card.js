function showCard(id){
    $.ajax({
        url: "http://"+window.location.host+":8080/transactions/"+id,
        headers: {
            "Authorization": "Bearer " + localStorage.getItem('bearer')
        },
        type: 'GET',
        dataType: 'json',
        success: function(card) {
            document.querySelector("#buyCard");
            let container_card= document.querySelector("#container_card");
            container_card.innerHTML = `
                <div class="ui special cards">
                    <div class="card">
                        <div class="content">
                            <div class="ui grid">
                                <div class="three column row">
                                    <div class="column">
                                        <h5>${card.card.tc.name}</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="image imageCard">
                                <div class="blurring dimmable image">
                                    <div class="ui fluid image">
                                        <a class="ui left corner label">
                                            ${card.card.tc.family}
                                        </a>
                                        <img id="cardImgId" class="ui centered image" src="${card.card.tc.avatarURL}">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="ui form tiny">
                                    <div class="field">
                                        <label id="cardNameId"></label>
                                        <textarea id="cardDescriptionId" class="overflowHiden" readonly="" rows="2">${card.card.tc.description}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <i class="heart outline icon"></i><span id="cardHPId"> HP ${card.card.tc.hp}</span> 
                                <div class="right floated ">
                                        <span id="cardEnergyId">Energy ${card.card.tc.energy}</span>
                                    <i class="lightning icon"></i>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <span class="right floated">
                                        <span id="cardAttackId"> Attack ${card.card.tc.attack}</span> 
                                    <i class=" wizard icon"></i>
                                </span>
                                <i class="protect icon"></i>
                                <span id="cardDefenceId">Defense ${card.card.tc.defense}</span> 
                            </div>
                            <div class="ui bottom attached button" id="buyCard" onclick="buy(${card.id})">
                                <i class="money icon"></i>
                                Buy for <span id="cardPriceId"> ${card.card.tc.price}$</span>
                            </div>
                        </div>
                    </div>
                </div>
            `;
        },
        error: function(errorThrown){
            swal({
                title: 'Buy error',
                text: errorThrown.responseJSON.message,
                icon: 'error',
                timer: 1000,
                button:false
            })        
        }
    })
}

function buy(id){  
    $.ajax({
        url: 'http://'+window.location.host+':8080/transactions/'+id,
        headers: {
            "Authorization": "Bearer " + localStorage.getItem('bearer')
        },
        type: "PUT",
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        success: function() {
            swal({
                title: 'Buy Success',
                text: 'Redirecting...',
                icon: 'success',
                timer: 1000,
                button:false
            }).then(() => {
                window.location.href = "./sell.html";
            })
        },
        error: function(errorThrown){
            swal({
                title: 'Buy error',
                text: errorThrown.responseJSON.message,
                icon: 'error',
                timer: 1000,
                button:false
            })        
        }
    })
}