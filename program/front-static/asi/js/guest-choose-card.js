const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const room_id = urlParams.get('room_id');

function showCard(id){
    $.ajax({
        url: "http://"+window.location.host+":8080/cards/"+id,
        headers: {
            "Authorization": "Bearer " + localStorage.getItem('bearer')
        },
        type: 'GET',
        dataType: 'json',
        success: function(card) {
            document.querySelector("#chooseCard");
            let container_card= document.querySelector("#container_card");
            container_card.innerHTML = `
                <div class="ui special cards">
                    <div class="card">
                        <div class="content">
                            <div class="ui grid">
                                <div class="three column row">
                                    <div class="column">
                                        <h5>${card.tc.name}</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="image imageCard">
                                <div class="blurring dimmable image">
                                    <div class="ui fluid image">
                                        <a class="ui left corner label">
                                            ${card.tc.family}
                                        </a>
                                        <img id="cardImgId" class="ui centered image" src="${card.tc.avatarURL}">
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="ui form tiny">
                                    <div class="field">
                                        <label id="cardNameId"></label>
                                        <textarea id="cardDescriptionId" class="overflowHiden" readonly="" rows="2">${card.tc.description}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <i class="heart outline icon"></i><span id="cardHPId"> HP ${card.tc.hp}</span> 
                                <div class="right floated ">
                                        <span id="cardEnergyId">Energy ${card.tc.energy}</span>
                                    <i class="lightning icon"></i>
                                    
                                </div>
                            </div>
                            <div class="content">
                                <span class="right floated">
                                        <span id="cardAttackId"> Attack ${card.tc.attack}</span> 
                                    <i class=" wizard icon"></i>
                                </span>
                                <i class="protect icon"></i>
                                <span id="cardDefenceId">Defense ${card.tc.defense}</span> 
                            </div>
                            <div class="ui bottom attached button" id="chooseCard" onclick="choose(${room_id},${card.id})">
                                <i class="gavel icon"></i>
                                Play with <span id="cardPriceId"> ${card.tc.name}</span>
                            </div>
                        </div>
                    </div>
                </div>
            `;
        },
        error: function(errorThrown){
            swal({
                title: 'Choose error',
                text: errorThrown.responseJSON.message,
                icon: 'error',
                timer: 1000,
                button:false
            })        
        }
    })
}

function choose(room_id,card_id){
    var fight = {
        BCardId:card_id,
    }

    $.ajax({
        url: 'http://'+window.location.host+':8080/fights/'+room_id,
        headers: {
            "Authorization": "Bearer " + localStorage.getItem('bearer')
        },
        type: "PUT",
        dataType: 'json',
        data: JSON.stringify(fight),
        contentType: 'application/json;charset=UTF-8',
        success: function() {
            swal({
                title: 'Join Fight Success',
                text: 'Redirecting...',
                icon: 'success',
                timer: 1000,
                button:false
            }).then(() => {
                window.location.href = "./room.html?room_id="+room_id;
            })
        },
        error: function(errorThrown){
            swal({
                title: 'Join Fight error',
                text: errorThrown.responseJSON.message,
                icon: 'error',
                timer: 1000,
                button:false
            })        
        }
    })
}