$.ajax({
    url: "http://"+window.location.host+":8080/transactions?status=0",
    headers: {
        "Authorization": "Bearer " + localStorage.getItem('bearer')
    },
    type: 'GET',
    dataType: 'json',
    success: function(cardList) {
        let template_cards = document.querySelector("#buyCards");
        for(const card of cardList){
            let clone_cards = document.importNode(template_cards.content, true);
            newContent= clone_cards.firstElementChild.innerHTML
                        .replace(/{{id}}/g, card.id)
                        .replace(/{{family_name}}/g, card.card.tc.family)
                        .replace(/{{name}}/g, card.card.tc.name)
                        .replace(/{{hp}}/g, card.card.tc.hp)
                        .replace(/{{affinity}}/g, card.card.tc.affinity)
                        .replace(/{{energy}}/g, card.card.tc.energy)
                        .replace(/{{attack}}/g, card.card.tc.attack)
                        .replace(/{{defense}}/g, card.card.tc.defense)
                        .replace(/{{price}}/g, card.card.tc.price);
            clone_cards.firstElementChild.innerHTML= newContent;
        
            let container_cards= document.querySelector("#container_cards");
            container_cards.appendChild(clone_cards);
            
        }
        $( "#container_cards tr" ).on( "click", function( event ) {
            var cardID = $(this).find("td").eq(0).html();
            if (cardID) {
                showCard(cardID);
            }
        });
    },
    error: function(errorThrown){
        swal({
            title: 'Sell error',
            text: 'An error occured',
            icon: 'error',
            timer: 1000,
            button:false
        })        
    }
})