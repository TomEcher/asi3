$.ajax({
    url: "http://"+window.location.host+":8080/users",
    headers: {
        "Authorization": "Bearer " + localStorage.getItem('bearer')
    },
    type: 'GET',
    dataType: 'json',
    success: function(user) {        
        let template_user = document.querySelector("#user");
        let clone_user = document.importNode(template_user.content, true);
        window.userid =   user.id;
        newContent= clone_user.firstElementChild.innerHTML
                    .replace(/{{fname}}/g, user.fname)
                    .replace(/{{lname}}/g, user.lname)
                    .replace(/{{pseudo}}/g, user.pseudo)
                    .replace(/{{balance}}/g, user.balance)
                    
        clone_user.firstElementChild.innerHTML= newContent;


        let userContainer= document.querySelector("#userContainer");
        userContainer.appendChild(clone_user);
        const user_id = user.id;
    },
    error: function(){
        swal({
            title: 'Login Error',
            text: 'Redirecting...',
            icon: 'error',
            timer: 1000,
            button:false
        }).then(() => {
            window.location.href = "./login.html";
        })
    }
})

function logout(){
    localStorage.removeItem('bearer');
    swal({
        title: 'Logout Success',
        text: 'Redirecting...',
        icon: 'success',
        timer: 1000,
        button:false
    }).then(() => {
        window.location.href = "./login.html";
    })
}