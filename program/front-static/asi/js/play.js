$.ajax({
    url: "http://"+window.location.host+":8080/fights/",
    headers: {
        "Authorization": "Bearer " + localStorage.getItem('bearer')
    },
    type: 'GET',
    dataType: 'json',
    success: function(roomList) {
        let template_rooms = document.querySelector("#chooseFight");
        for(const room of roomList){
            let clone_rooms = document.importNode(template_rooms.content, true);
            newContent= clone_rooms.firstElementChild.innerHTML
                        .replace(/{{id}}/g, room.id)
                        .replace(/{{name_room}}/g, room.FightName)
                        .replace(/{{opponent_id}}/g, room.auserid)
                        .replace(/{{bet}}/g, room.bet)
            clone_rooms.firstElementChild.innerHTML= newContent;
        
            let container_rooms= document.querySelector("#container_rooms");
            container_rooms.appendChild(clone_rooms);
            
        }
    },
    error: function(errorThrown){
        swal({
            title: 'Room error',
            text: 'An error occured',
            icon: 'error',
            timer: 1000,
            button:false
        })        
    }
})

function join(id){  
    $.ajax({
        url: 'http://'+window.location.host+':8080/fight/'+id,
        headers: {
            "Authorization": "Bearer " + localStorage.getItem('bearer')
        },
        type: "PUT",
        dataType: 'json',
        contentType: 'application/json;charset=UTF-8',
        success: function() {
            swal({
                title: 'Join Fight Success',
                text: 'Redirecting...',
                icon: 'success',
                timer: 1000,
                button:false
            }).then(() => {
                window.location.href = "./room.html?room_id="+id;
            })
        },
        error: function(errorThrown){
            swal({
                title: 'Join Fight error',
                text: errorThrown.responseJSON.message,
                icon: 'error',
                timer: 1000,
                button:false
            })        
        }
    })
}
