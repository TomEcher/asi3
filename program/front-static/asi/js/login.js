
function send(event) {
    event.preventDefault()

    var user = {
        login:$("#pseudo").val(),
        password:$("#password").val(),
    }

    $.ajax({
        url: 'http://'+window.location.host+':8080/auth',
        type: "POST",
        dataType: 'json',
        data: JSON.stringify(user),
        contentType: 'application/json;charset=UTF-8', 
        success: function(data) {
            if(localStorage.getItem('bearer')){
                localStorage.removeItem('bearer');
            }
            localStorage.setItem('bearer', data.token);
            swal({
                title: 'Login Success',
                text: 'Redirecting...',
                icon: 'success',
                timer: 1000,
                button:false
            }).then(() => {
                window.location.href = "./home.html";
            })
        },
        error: function(){
            sweetAlert("Oops...", "An error occured", "error");
        }
    })
    
}