const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const room_id = urlParams.get('room_id');

setInterval(function() {
    $.ajax({
        url: "http://"+window.location.host+":8080/fights/"+room_id,
        headers: {
            "Authorization": "Bearer " + localStorage.getItem('bearer')
        },
        type: 'GET',
        dataType: 'json',
        success: function(fightInfo) {
            console.log(fightInfo);
            let fight_status = fightInfo.status;
            let auser_id = fightInfo.auserid;
            let buser_id = fightInfo.buserid;
            if(fight_status == 0){
                let container_fight= document.querySelector("#container_fight");
                container_fight.innerHTML = `
                    <div class="ui icon message">
                        <i class="user loading icon"></i>
                        <div class="content">
                            <div class="header">Get ready</div>
                            <p>Wait for someone to join.</p>
                        </div>
                    </div>
                `;
            }
            else if(fight_status == 2){
                fight_over();
                if(auser_id==window.userid){
                    user_won();
                }
                else{
                    user_lost();
                }
            }
            else if(fight_status == 3){
                fight_over();
                if(buser_id==window.userid){
                    user_won();
                }
                else{
                    user_lost();
                }
            }
        },
        error: function(errorThrown){
            swal({
                title: 'Fight error',
                text: 'An error occured',
                icon: 'error',
                timer: 1000,
                button:false
            })        
        }
    })  
}, 5000);

function fight_over(){
    let container_fight= document.querySelector("#container_fight");
    container_fight.innerHTML = `
        <div class="ui icon message">
            <i class="flag checkered icon"></i>
            <div class="content">
                <div class="header">Breathe</div>
                <p>The fight is over.</p>
            </div>
        </div>
    `;
}

function user_won(){
    swal({
        title: 'Congratulations',
        text: 'You have won the fight',
        icon: 'success',
        timer: 3000,
        button:false
    }).then(() => {
        window.location.href = "./home.html";
    })
}

function user_lost(){
    swal({
        title: 'Too bad',
        text: 'You have lost the fight',
        icon: 'error',
        timer: 3000,
        button:false
    }).then(() => {
        window.location.href = "./home.html";
    })
}