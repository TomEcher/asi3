function send(event) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const card_id = urlParams.get('card_id');

    event.preventDefault()

    var fight = {
        ACardId:parseInt(card_id),
        FightName:$("#name").val(),
        Bet:parseInt($("#bet").val()),
    }
    $.ajax({
        url: 'http://'+window.location.host+':8080/fights',
        headers: {
            "Authorization": "Bearer " + localStorage.getItem('bearer')
        },
        type: "POST",
        dataType: 'json',
        data: JSON.stringify(fight),
        contentType: 'application/json;charset=UTF-8',
        success: function(data) {
            swal({
                title: 'Create Room Success',
                text: 'Redirecting...',
                icon: 'success',
                timer: 1000,
                button:false
            }).then(() => {
                console.log(data);
                window.location.href = "./room.html?room_id="+data.message;
            })
        },
        error: function(errorThrown){
            if(errorThrown.responseJSON.status==0){
                sweetAlert("Oops...", errorThrown.responseJSON.message, "error");
            }
            else{
                sweetAlert("Oops...", "An error occured", "error");
            }
        }
    })
}