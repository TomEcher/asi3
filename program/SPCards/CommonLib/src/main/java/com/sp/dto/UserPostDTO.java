package com.sp.dto;

public class UserPostDTO {
	public String lname;
	public String fname;
	public String pseudo;
	public String password;
	
	
	public UserPostDTO(String lname, String fname, String pseudo, String password) {
		super();
		this.lname = lname;
		this.fname = fname;
		this.pseudo = pseudo;
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserPostDTO() {
	}

	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
}
