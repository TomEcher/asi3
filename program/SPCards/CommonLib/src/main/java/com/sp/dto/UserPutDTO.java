package com.sp.dto;

public class UserPutDTO {
	public float balance;
	public long id_user;
	
	
	
	public UserPutDTO() {
		super();
	}
	
	public UserPutDTO(float balance, long id_user) {
		super();
		this.balance = balance;
		this.id_user = id_user;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public long getId_user() {
		return id_user;
	}
	public void setId_user(long id_user) {
		this.id_user = id_user;
	}
	
	

}
