package com.sp.mapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.dto.CardClientDTO;
import com.sp.dto.CardDTO;

import com.sp.dto.TemplateCardDTO;
import com.sp.dto.UserDTO;
import com.sp.model.Card;
import com.sp.model.TemplateCard;
import com.sp.service.TemplateCardService;

@Component
public class CardMapper {
	@Autowired
	TemplateCardService ts;

	@Autowired
	TemplateCardMapper tcm;

	public CardDTO toDTO(Card card){
		TemplateCardDTO tcDTO = tcm.toDTO(card.getTemplateCard());
		return new CardDTO(card.getId(), tcDTO);
	}
	
	public Card toModel(Card card, UserDTO userDTO){
		TemplateCard tc = ts.getTemplateCard(card.getTemplateCard().getId());
		UserDTO u = userDTO;
		return new Card(u.getId(),tc);
		
	}
	
	public Card ClientToModel(CardClientDTO cardDTO) {
		TemplateCard tc = ts.getTemplateCard(cardDTO.getTemplateCardId());
		return new Card(cardDTO.getUserId(),tc);
	}
}
