package com.sp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import com.sp.model.Card;
public interface CardRepository extends CrudRepository<Card, Integer> {

	public List<Card> findAll();
	public Optional<Card> findById(Long id);
	public List<Card> findByOwner(Long id_user);
}
