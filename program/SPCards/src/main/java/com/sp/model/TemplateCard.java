package com.sp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "templatecards")
public class TemplateCard {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    private Long id;
	
	@Column(name = "name", unique=true, insertable=true, updatable=true, nullable=false)
    public String name;
	
	@Column(name = "description", insertable=true, updatable=true, nullable=false)
	public String description;
	
	@Column(name = "family", insertable=true, updatable=true, nullable=false)
	public String family;
	
	@Column(name = "affinity", insertable=true, updatable=true, nullable=false)
	public String affinity;
	
	@Column(name = "hp", insertable=true, updatable=true, nullable=false)
	public Integer hp;
	
	@Column(name = "attack", insertable=true, updatable=true, nullable=false)
	public Integer attack;
	
	@Column(name = "defense", insertable=true, updatable=true, nullable=false)
	public Integer defense;
	
	@Column(name = "energy", insertable=true, updatable=true, nullable=false)
	public Integer energy;
	
	@Column(name = "price", insertable=true, updatable=true, nullable=false)
	public Float price;
	
	@Column(name = "avatarURL", insertable=true, updatable=true, nullable=false)
	public String avatarURL;
	
	public TemplateCard() {
	}
	public TemplateCard(String name, String description,String family, String affinity, Integer hp, Integer attack, Integer defense, Integer energy, Float price, String avatar) {
		this.name = name;
		this.description = description;
		this.family =family;
		this.affinity = affinity;
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.energy = energy;
		this.price = price;
		this.avatarURL = avatar;

	}

	public String getAvatarURL() {
		return avatarURL;
	}
	public void setAvatarURL(String avatarURL) {
		this.avatarURL = avatarURL;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id= id;
	}
	
	public String getName() {
		return name;
	}
	public void setPseudo(String name) {
		this.name=name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description=description;
	}
	
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family=family;
	}
	
	public String getAffinity() {
		return affinity;
	}
	public void setAffinity(String affinity) {
		this.affinity=affinity;
	}
	
	public Integer getHp() {
		return hp;
	}
	public void setHp(Integer hp) {
		this.hp=hp;
	}
	
	public Integer getAttack() {
		return attack;
	}
	public void setAttack(Integer attack) {
		this.attack=attack;
	}
	
	public Integer getDefense() {
		return defense;
	}
	public void setDefense(Integer defense) {
		this.defense=defense;
	}
	
	public Integer getEnergy() {
		return defense;
	}
	public void setEnergy(Integer energy) {
		this.energy=energy;
	}
	
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price=price;
	}
	
}
