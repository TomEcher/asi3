package com.sp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpAppCards {
	
	public static void main(String[] args) {
		SpringApplication.run(SpAppCards.class,args);
	}
}
