package com.sp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.TemplateCard;
import com.sp.repository.TemplateCardRepository;

@Service
public class TemplateCardService {
	@Autowired
	TemplateCardRepository tcRepository;
	
    /**
    *
    * Création d'une template card
    *
    */	
	public void addCard(TemplateCard tc) {
		TemplateCard createdTemplateCard= tcRepository.save(tc);
		System.out.println(createdTemplateCard);
	}
	
    /**
    *
    * Récupération d'une template card à partir d'un ID
    *
    */	
	public TemplateCard getTemplateCard(Long id) {
		Optional<TemplateCard> cOpt = tcRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}
	
    /**
    *
    * Récupération de l'ensemble des templates card
    *
    */		
	public List<TemplateCard> getAllTemplateCard() {
		List<TemplateCard> cards = tcRepository.findAll();
		return cards;
	}

}
