package com.sp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.Card;
import com.sp.model.TemplateCard;
import com.sp.repository.CardRepository;
import com.sp.repository.TemplateCardRepository;

@Service
public class CardService {
	@Autowired
	CardRepository cRepository;
	
	@Autowired
	TemplateCardRepository tcRepository;
	
	
	public void addCard(Card c) {
		cRepository.save(c);
	}
	
	
	
    /**
    *
    * Récupération des cartes par une ID
    *
    */	
	public Card getCard(Long id) {
		Optional<Card> cOpt =cRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}
	
    /**
    *
    * Récupération de l'ensemble des cartes
    *
    */	
	public List<Card> getAllCard() {
		List<Card> cards = cRepository.findAll();
		return cards;
	}
	

    /**
    *
    * Récupération de l'ensemble des cartes d'un utilisateur
    *
    */	
	public List<Card> getAllCardByUser(Long id_user) {
		List<Card> cards = cRepository.findByOwner(id_user);
		return cards;
	}
	
	public void Give5CardToID(Long id_user) {
		List<TemplateCard> templatecard = tcRepository.FindFiveCardRandom();

		for (int i = 0; i < templatecard.size(); i++) {
			Card card = new Card();
			card.setOwner(id_user);
			card.setTemplateCard(templatecard.get(i));
			cRepository.save(card);
		}
	}

}
