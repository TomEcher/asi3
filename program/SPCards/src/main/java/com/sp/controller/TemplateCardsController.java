package com.sp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.sp.dto.MessageResponseDTO;
import com.sp.dto.TemplateCardClientDTO;
import com.sp.dto.TemplateCardDTO;
import com.sp.mapper.TemplateCardMapper;
import com.sp.model.TemplateCard;
import com.sp.service.TemplateCardService;

@RestController
@CrossOrigin(origins = "*")
public class TemplateCardsController {
	

    @Autowired
    TemplateCardService tcService;
    @Autowired
    TemplateCardMapper tcMapper;
    
	@RequestMapping(method=RequestMethod.GET,value="/templatecards")
	public ResponseEntity<List<TemplateCardDTO>> getAllTemplateCards() {
		
		List<TemplateCard> TemplateCard = tcService.getAllTemplateCard();
		List<TemplateCardDTO> TemplateCardList = new ArrayList<TemplateCardDTO>();
		for (int i = 0; i < TemplateCard.size(); i++) {
			TemplateCardList.add(tcMapper.toDTO(TemplateCard.get(i)));
		  }
		return new ResponseEntity<List<TemplateCardDTO>>(TemplateCardList, HttpStatus.ACCEPTED);

		
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/templatecards")
	public ResponseEntity<MessageResponseDTO> AddTemplateCards(@RequestBody TemplateCardClientDTO TemplateCardDTO) {
		TemplateCard tc = tcMapper.toModel(TemplateCardDTO);
		try {
			tcService.addCard(tc);
		}catch(Exception e ){
            System.out.println("Duplication d'utilisateur");
    		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "template card creation failed"), HttpStatus.CONFLICT);
        }
		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "succeded template card creation"), HttpStatus.ACCEPTED);

	}
}
