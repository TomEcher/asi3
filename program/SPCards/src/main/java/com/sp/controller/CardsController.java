package com.sp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;import com.sp.dto.CardClientDTO;
import com.sp.dto.CardDTO;
import com.sp.dto.CardS2SDTO;
import com.sp.dto.MessageResponseDTO;
import com.sp.dto.UserDTO;
import com.sp.mapper.CardMapper;
import com.sp.model.Card;
import com.sp.service.CardService;
import com.sp.tools.UserLoginTools;

@RestController
@CrossOrigin(origins = "*")
public class CardsController {

    @Autowired
    CardService cService;

    @Autowired
    CardMapper cMapper;
   

    @Autowired
    HttpServletRequest request;

    @Autowired
    UserLoginTools ulTools;
	/*
	 * Client EndPoint
	 */
	
	@RequestMapping(method=RequestMethod.GET,value="/cards")
	public ResponseEntity<List<CardDTO>> getCardOfUser() {
		UserDTO monUser = ulTools.getUserByToken(request.getHeader("Authorization"));
		
		List<Card> Card = cService.getAllCardByUser(monUser.getId());
		List<CardDTO> CardList = new ArrayList<CardDTO>();
		for (int i = 0; i < Card.size(); i++) {
			CardList.add(cMapper.toDTO(Card.get(i)));
		  }
		return new ResponseEntity<List<CardDTO>>(CardList, HttpStatus.ACCEPTED);

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards")
	public ResponseEntity<MessageResponseDTO> AddCard(@RequestBody CardClientDTO CardClientDTO) {
		UserDTO monUser = ulTools.getUserByToken(request.getHeader("Authorization"));
		Card card = cMapper.ClientToModel(CardClientDTO);
		if(card.getTemplateCard() != null && card.getOwner() != null) {
			cService.addCard(card);
			
			return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, "successful card creation"), HttpStatus.ACCEPTED);
		}else {
			return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "transaction card failed : some arguments are wrong"), HttpStatus.CONFLICT);
		}
	}
	

	/*
	 * S2S EndPoint
	 */

	@RequestMapping(method=RequestMethod.GET,value="/cards/{id}")
	public ResponseEntity<CardDTO> getCardByID(@PathVariable String id) {
        Card c=cService.getCard(Long.parseLong(id));
        
		return new ResponseEntity<CardDTO>(cMapper.toDTO(c), HttpStatus.ACCEPTED);

	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/cards")
	public ResponseEntity<MessageResponseDTO> UpdateCard(@RequestBody CardS2SDTO CardS2SDTO) {
		
		try {
			Card card = cService.getCard(CardS2SDTO.getCardId());
			card.setOwner(CardS2SDTO.getUserId());
			cService.addCard(card);
			
		}catch(Exception e ){
			return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "change owner card succed : some arguments are wrong"), HttpStatus.CONFLICT);
        }

		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, "change owner card succed"), HttpStatus.ACCEPTED);
	
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cards/give")
	public ResponseEntity<MessageResponseDTO> Give5Cards(@RequestBody UserDTO userDTO) {

		try {
			cService.Give5CardToID(userDTO.getId());
			
		}catch(Exception e ){
			return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "transaction card failed : some arguments are wrong"), HttpStatus.CONFLICT);
        }
		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, "succed give 5 card"), HttpStatus.CONFLICT);
	}
}
