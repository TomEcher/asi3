package com.sp.dto;

import java.util.Date;



public class FightDTO {

	public Long id;
	public Long auserid;
	public Long buserid;
	public Long acardid;
	public Long bcardid;
	public Float bet;
	
	public Date updateAt;
	
	public long status;
	public String FightName;

	public FightDTO(Long id, Long aUserId, Long bUserId, Long aCardId, Long bUCardId, Date updateAt, long status, float bet, String fn) {
		super();
		this.id = id;
		this.auserid = aUserId;
		this.buserid = bUserId;
		this.acardid = aCardId;
		this.bcardid = bUCardId;
		this.updateAt = updateAt;
		this.status = status;
		this.bet = bet;
		this.FightName = fn;
	}
	
	

	public FightDTO() {
		super();
	}


	public String getFightName() {
		return FightName;
	}



	public void setFightName(String fightName) {
		FightName = fightName;
	}

	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Long getAuserid() {
		return auserid;
	}



	public void setAuserid(Long auserid) {
		this.auserid = auserid;
	}



	public Long getBuserid() {
		return buserid;
	}



	public void setBuserid(Long buserid) {
		this.buserid = buserid;
	}



	public Long getAcardid() {
		return acardid;
	}



	public void setAcardid(Long acardid) {
		this.acardid = acardid;
	}



	public Long getBcardid() {
		return bcardid;
	}



	public void setBcardid(Long bcardid) {
		this.bcardid = bcardid;
	}



	public Float getBet() {
		return bet;
	}



	public void setBet(Float bet) {
		this.bet = bet;
	}



	public Date getUpdateAt() {
		return updateAt;
	}



	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}



	public long getStatus() {
		return status;
	}



	public void setStatus(long status) {
		this.status = status;
	}





	
	
	
}
