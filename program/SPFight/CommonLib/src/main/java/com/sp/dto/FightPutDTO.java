package com.sp.dto;



public class FightPutDTO {
	
	public Long FightId;
	public Long BCardId;
	

	
	public FightPutDTO() {
		super();
	}



	public FightPutDTO(Long fightId, Long bUserId, Long bCardId) {
		super();
		FightId = fightId;
		BCardId = bCardId;
	}

	public Long getFightId() {
		return FightId;
	}

	public void setFightId(Long fightId) {
		FightId = fightId;
	}


	public Long getBCardId() {
		return BCardId;
	}

	public void setBCardId(Long bCardId) {
		BCardId = bCardId;
	}



	
	
}
