package com.sp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.sp.model.Fight;

public interface FightRepository extends CrudRepository<Fight, Integer> {

	public List<Fight> findAll();
	public Optional<Fight> findById(Long id);
	public List<Fight> findByStatus(Long id);
}
