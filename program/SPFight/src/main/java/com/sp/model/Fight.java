package com.sp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Fights")
public class Fight {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    private Long id;
	
	public Long AUserId;
	public Long BUserId;
	public Long ACardId;
	public Float Bet;
	public Long BUCardId;
	
	public Date updateAt;
	
	public long status;

	public String FightName;

	public Fight() {
		super();
	}


	
	
	public Fight( Long aUserId, Long bUserId, Long aCardId, Float bet, Long bUCardId, Date updateAt,
			long status, String fightName) {
		super();
		this.id = id;
		AUserId = aUserId;
		BUserId = bUserId;
		ACardId = aCardId;
		Bet = bet;
		BUCardId = bUCardId;
		this.updateAt = updateAt;
		this.status = status;
		FightName = fightName;
	}




	public String getFightName() {
		return FightName;
	}




	public void setFightName(String fightName) {
		FightName = fightName;
	}




	public Float getBet() {
		return Bet;
	}

	public void setBet(Float bet) {
		Bet = bet;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAUserId() {
		return AUserId;
	}

	public void setAUserId(Long aUserId) {
		AUserId = aUserId;
	}

	public Long getBUserId() {
		return BUserId;
	}

	public void setBUserId(Long bUserId) {
		BUserId = bUserId;
	}

	public Long getACardId() {
		return ACardId;
	}

	public void setACardId(Long aCardId) {
		ACardId = aCardId;
	}

	public Long getBUCardId() {
		return BUCardId;
	}

	public void setBUCardId(Long bUCardId) {
		BUCardId = bUCardId;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public long getStatus() {
		return status;
	}

	public void setStatus(long status) {
		this.status = status;
	}

	
}
