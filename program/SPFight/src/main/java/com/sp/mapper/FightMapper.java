package com.sp.mapper;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.dto.FightDTO;
import com.sp.dto.FightPostDTO;
import com.sp.dto.FightPutDTO;
import com.sp.model.Fight;
import com.sp.service.FightService;

@Component
public class FightMapper {

	@Autowired
    FightService fService;
	
	public FightDTO toDTO(Fight fight){
		System.out.println(fight.getId());
		return new FightDTO(fight.getId(),fight.getAUserId(), fight.getBUserId(), fight.getACardId(), fight.getBUCardId(), fight.getUpdateAt(), fight.getStatus(), fight.getBet(), fight.getFightName());

	}
	
	public Fight toModel(FightPostDTO fight){

		return new Fight(fight.getAUserId(), null, fight.getACardId(),fight.getBet(), null, new Date(), (long) 0, fight.getFightName());

	}
	
	public Fight PutToModel(FightPutDTO fightDTO, Long id){
		Fight fight = fService.getFight(id);
		fight.setBUCardId(fightDTO.getBCardId());
		return fight;

	}

}
