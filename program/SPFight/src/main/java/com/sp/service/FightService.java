package com.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;

import com.sp.dto.CardDTO;
import com.sp.dto.MessageResponseDTO;
import com.sp.dto.UserLoginDTO;
import com.sp.dto.UserPutDTO;
import com.sp.model.Fight;
import com.sp.repository.FightRepository;

@Service
public class FightService{
	@Autowired
	FightRepository fRepository;
		
    /**
    *
    * Cr�ation d'un utilisateur
    *
    */	
	public void addFight(Fight f) {
        try {
    		Fight fight = fRepository.save(f);
        }catch(Exception e ){
            System.out.println("Duplication de Fight");
        }
		
	}
	
	
    /**
    *
    * R�cup�ration des informations utilisateurs � partir de son ID
    *
    */	
	public Fight getFight(Long id) {
		Optional<Fight> uOpt = fRepository.findById(id);
		if (uOpt.isPresent()) {
			return uOpt.get();
		}else {
			return null;
		}
	}
	
    /**
    *
    * R�cup�ration des informations utilisateurs � partir de son ID
    *
    */	
	public List<Fight> getAllFightActive() {
		List<Fight> fights = fRepository.findByStatus((long) 0);
		
		return fights;
	}
	
	
	public Fight whoIsTheWinner(Fight fight) {
		CardDTO card1 = getCardbyID(fight.getACardId());
		CardDTO card2 = getCardbyID(fight.getBUserId());
		
		return fight;
	}
	
	public CardDTO getCardbyID(Long card_id) {
		RestTemplate restTemplate = new RestTemplate();

			System.out.println("http://rproxy:8080/cards/"+card_id);
			CardDTO card = restTemplate.getForObject("http://rproxy:8080/cards/"+card_id, CardDTO.class);
			return card;

	}
	
	public void changeBalance(Long user_id, float balance) {
		RestTemplate restTemplate = new RestTemplate();
		UserPutDTO user = new UserPutDTO();
		user.setId_user(user_id);
		user.setBalance(balance);
		try {
		    HttpEntity<UserPutDTO> entity = new HttpEntity<UserPutDTO>(user);
		    restTemplate.exchange("http://rproxy:8080/users/", HttpMethod.PUT, entity, UserPutDTO.class);
		}catch(Exception e ){

			System.out.println("UPDATE ERROR");
        }
		
	}

}
