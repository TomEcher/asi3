package com.sp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.sp.dto.MessageResponseDTO;
import com.sp.dto.TransactionDTO;
import com.sp.dto.UserDTO;
import com.sp.dto.UserGetDTO;
import com.sp.dto.UserLoginDTO;
import com.sp.dto.UserPostDTO;
import com.sp.dto.UserPutDTO;
import com.sp.dto.CardDTO;
import com.sp.dto.FightDTO;
import com.sp.dto.FightPostDTO;
import com.sp.dto.FightPutDTO;
import com.sp.mapper.FightMapper;
import com.sp.model.Fight;
import com.sp.repository.FightRepository;
import com.sp.service.FightService;
import com.sp.tools.CardTools;
import com.sp.tools.UserLoginTools;

@RestController
@CrossOrigin(origins = "*")
public class FightController {
	@Autowired
    FightService fService;
	
	@Autowired
    FightRepository fRepository;

    @Autowired
    FightMapper fMapper;
    
    @Autowired
    UserLoginTools ulTools;
    @Autowired
    CardTools cTools;
    
    @Autowired
    HttpServletRequest request;
    
	/*
	 * WEB EndPoint
	 */
	@RequestMapping(method=RequestMethod.GET,value="/fights")
	public ResponseEntity<List<FightDTO>> getListFight(){
		
		List<Fight> afight = fService.getAllFightActive();
		List<FightDTO> FightList = new ArrayList<FightDTO>();
		
		for (int i = 0; i < afight.size(); i++){
			FightList.add(fMapper.toDTO(afight.get(i)));
		}
		return new ResponseEntity<List<FightDTO>>(FightList, HttpStatus.ACCEPTED);

	}
	
	
	/*
	 * WEB EndPoint
	 */
	@RequestMapping(method=RequestMethod.GET,value="/fights/{id}")
	public ResponseEntity<FightDTO> getFight(@PathVariable String id){
		Fight fight = null;
		try {
			fight = fService.getFight(Long.parseLong(id));

		}catch(Exception e ){
			return new ResponseEntity<FightDTO>(HttpStatus.CONFLICT);

		}
		return new ResponseEntity<FightDTO>(fMapper.toDTO(fight), HttpStatus.ACCEPTED);



	}
	
	@RequestMapping(method=RequestMethod.POST,value="/fights")
	public ResponseEntity<MessageResponseDTO> addUser(@RequestBody FightPostDTO fightDTO){
		Fight fight = null;
		UserDTO user = ulTools.getUserByToken(request.getHeader("Authorization"));

		try {
			fight = fMapper.toModel(fightDTO);
			fight.setAUserId(user.getId());
			if(user.getBalance() < fight.getBet()) {
				return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "no money for play"), HttpStatus.CONFLICT);
			}
			fService.addFight(fight);
		}catch(Exception e ){
            System.out.println("Duplication d'utilisateur");
    		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "fight creation failed"), HttpStatus.CONFLICT);
		
        }
		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, ""+fight.getId()), HttpStatus.ACCEPTED);
	}		

	@RequestMapping(method=RequestMethod.PUT,value="/fights/{id}")
	public ResponseEntity<MessageResponseDTO> editUserByID(@RequestBody FightPutDTO fpd, @PathVariable String id){
		UserDTO user = ulTools.getUserByToken(request.getHeader("Authorization"));
		System.out.println(id);
		Fight monFight = fMapper.PutToModel(fpd, Long.parseLong(id));
		monFight.setBUserId(user.getId());
		System.out.println(user.getBalance());
		if(user.getBalance() < monFight.getBet()) {
			return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "no money for play"), HttpStatus.CONFLICT);

		}

		CardDTO cardA = cTools.getUserByToken(monFight.getACardId());

		CardDTO cardB = cTools.getUserByToken(monFight.getBUCardId());
		
		
		Integer IndiceA = (cardA.getTc().getAttack()+cardA.getTc().getDefense()+cardA.getTc().getEnergy())/10;
		Integer IndiceB = (cardB.getTc().getAttack()+cardB.getTc().getDefense()+cardB.getTc().getEnergy())/10;

		if(cardA.getTc().getHp() % IndiceB > cardB.getTc().getHp() % IndiceA){
			monFight.setStatus(2);
			fService.changeBalance(monFight.getAUserId(), + monFight.getBet());
			fService.changeBalance(monFight.getBUserId(), - monFight.getBet());

		}else {
			monFight.setStatus(3);
			fService.changeBalance(monFight.getAUserId(), - monFight.getBet());
			fService.changeBalance(monFight.getBUserId(), + monFight.getBet());
		}
		fRepository.save(monFight);
		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, "successful fight change"), HttpStatus.ACCEPTED);
	}
	
	
	
}
