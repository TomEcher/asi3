package com.sp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpAppTokenManager {
	
	public static void main(String[] args) {
		SpringApplication.run(SpAppTokenManager.class,args);
	}
}
