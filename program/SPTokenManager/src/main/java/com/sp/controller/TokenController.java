package com.sp.controller;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import org.springframework.http.HttpEntity;

import com.sp.config.JwtTokenUtil;
import com.sp.dto.JwtResponseDTO;
import com.sp.dto.MessageResponseDTO;
import com.sp.dto.TokenDTO;
import com.sp.dto.UserDTO;
import com.sp.dto.UserLoginDTO;
import com.sp.dto.UserPostDTO;
import com.sp.service.TokenService;
import org.springframework.web.bind.annotation.CrossOrigin;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;


@RestController
@CrossOrigin(origins = "*")
public class TokenController {

    @Autowired
    TokenService tService;
    
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService jwtInMemoryUserDetailsService;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	@RequestMapping(method=RequestMethod.POST,value="/auth")
	public ResponseEntity<JwtResponseDTO> ConnexionVerifier(@RequestBody UserLoginDTO toto) {
		System.out.println(toto.getLogin());
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(toto.getPassword().getBytes(StandardCharsets.UTF_8));
			String encoded = Base64.getEncoder().encodeToString(hash);
			toto.setPassword(encoded);
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println(toto.getPassword());
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<UserLoginDTO> request = new HttpEntity<>(toto);
		MessageResponseDTO result;
		try {
			result = restTemplate.postForObject("http://rproxy:8080/users/login", request, MessageResponseDTO.class);
		}catch(Exception e ){
            System.out.println("Duplication d'utilisateur");
    		return new ResponseEntity<JwtResponseDTO>(new JwtResponseDTO(), HttpStatus.UNAUTHORIZED);

        }
		System.out.println(result.getMessage());
		final UserDetails userDetails = TokenService.loadUserByUsername(toto.getLogin(), toto.getPassword());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return new ResponseEntity<JwtResponseDTO>(new JwtResponseDTO(token), HttpStatus.ACCEPTED);

	}
	
	@RequestMapping(method=RequestMethod.GET,value="/auth/trust")
	public ResponseEntity<UserDTO> TokenVerifier(@RequestParam("token") String token) {
		
		
		String requestTokenHeader = token;
		System.out.println(token);
		String username = null;
		String jwtToken = null;
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader.substring(7);
			try {
				username = jwtTokenUtil.getUsernameFromToken(jwtToken);
			}catch(Exception e ){
	            System.out.println("Probleme JWT");
	    		return new ResponseEntity<UserDTO>(new UserDTO(), HttpStatus.UNAUTHORIZED);
	        }

		} else {
				System.out.println("JWT Token does not begin with Bearer String");
		}

		//Once we get the token validate it.
		if (username != null) {

			// if token is valid configure Spring Security to manually set authentication
			if (!jwtTokenUtil.isTokenExpired(jwtToken)) {
				
				
				RestTemplate restTemplate = new RestTemplate();
				System.out.println("http://rproxy:8080/users/"+username);
				try {
					UserDTO UserDTO = restTemplate.getForObject("http://rproxy:8080/users/"+username, UserDTO.class);
					return new ResponseEntity<UserDTO>(UserDTO, HttpStatus.ACCEPTED);
				}catch(Exception e ){
		            System.out.println("Duplication d'utilisateur");
		    		return new ResponseEntity<UserDTO>(new UserDTO(), HttpStatus.CONFLICT);

		        }

			}
		}
		return new ResponseEntity<UserDTO>(new UserDTO(), HttpStatus.UNAUTHORIZED);

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/auth/crypt")
	public ResponseEntity<UserLoginDTO> PasswordCrypt(@RequestBody UserLoginDTO user) throws NoSuchAlgorithmException {
		
		
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(user.getPassword().getBytes(StandardCharsets.UTF_8));
		String encoded = Base64.getEncoder().encodeToString(hash);
		
		user.setPassword(encoded);
		
		
		return new ResponseEntity<UserLoginDTO>(user, HttpStatus.ACCEPTED);


	}

}
