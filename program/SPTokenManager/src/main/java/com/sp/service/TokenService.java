package com.sp.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Service;


@Service
public class TokenService implements UserDetailsService{

	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	public String EncryptPassword(String password) {
		return bcryptEncoder.encode(password);
	}
	
	public static UserDetails loadUserByUsername(String username, String password) throws UsernameNotFoundException {
		return new org.springframework.security.core.userdetails.User(username, password,
				new ArrayList<>());
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
}
