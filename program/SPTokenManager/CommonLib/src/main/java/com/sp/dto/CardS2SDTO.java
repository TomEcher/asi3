package com.sp.dto;

public class CardS2SDTO {
	public Long userId;
	public Long cardId;
	
	public CardS2SDTO(Long user_id, Long card_id) {
		this.userId = user_id;
		this.cardId = card_id;
	}
	
	public CardS2SDTO() {}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCardId() {
		return cardId;
	}

	public void setCardId(Long cardId) {
		this.cardId = cardId;
	}

	
}
