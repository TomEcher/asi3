package com.sp.tools;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import com.sp.dto.CardDTO;
import com.sp.dto.UserDTO;

@Component
public class CardTools {
	public CardDTO getUserByToken(Long id) {
		RestTemplate restTemplate = new RestTemplate();
		CardDTO result;
	    Map<String, String> params = new HashMap<String, String>();
	    params.put("id", Long.toString(id));
		try {
			result = restTemplate.getForObject("http://rproxy:8080/cards/{id}", CardDTO.class, params);
			return result;
		}catch(Exception e ){
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

        }
	}
}
