package com.sp.dto;



public class TemplateCardDTO {

	public String name;
	public String description;
	public String family;
	public String affinity;
	public Integer hp;
	public Integer attack;
	public Integer defense;
	public Integer energy;
	public Float price;
	public String avatarURL;
	
	
	public TemplateCardDTO(String name, String description, String family, String affinity, Integer hp, Integer attack,
			Integer defense, Integer energy, Float price, String avatarURL) {
		this.name = name;
		this.description = description;
		this.family = family;
		this.affinity = affinity;
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.energy = energy;
		this.price = price;
		this.avatarURL = avatarURL;
	}
	public TemplateCardDTO() {
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getFamily() {
		return family;
	}


	public void setFamily(String family) {
		this.family = family;
	}


	public String getAffinity() {
		return affinity;
	}


	public void setAffinity(String affinity) {
		this.affinity = affinity;
	}


	public Integer getHp() {
		return hp;
	}


	public void setHp(Integer hp) {
		this.hp = hp;
	}


	public Integer getAttack() {
		return attack;
	}


	public void setAttack(Integer attack) {
		this.attack = attack;
	}


	public Integer getDefense() {
		return defense;
	}


	public void setDefense(Integer defense) {
		this.defense = defense;
	}


	public Integer getEnergy() {
		return energy;
	}


	public void setEnergy(Integer energy) {
		this.energy = energy;
	}


	public Float getPrice() {
		return price;
	}


	public void setPrice(Float price) {
		this.price = price;
	}


	public String getAvatarURL() {
		return avatarURL;
	}


	public void setAvatarURL(String avatarURL) {
		this.avatarURL = avatarURL;
	}
	
	
	
	
	
}
