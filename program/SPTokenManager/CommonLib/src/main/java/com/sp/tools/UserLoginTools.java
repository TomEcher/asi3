package com.sp.tools;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Component;

import com.sp.dto.JwtResponseDTO;
import com.sp.dto.MessageResponseDTO;
import com.sp.dto.UserDTO;
import com.sp.dto.UserLoginDTO;

import org.springframework.web.server.ResponseStatusException;
@Component
public class UserLoginTools {

	public UserDTO getUserByToken(String Token) {
		System.out.println(Token);
		RestTemplate restTemplate = new RestTemplate();
		UserDTO result;
	    Map<String, String> params = new HashMap<String, String>();
	    params.put("tokenValue", Token);
		try {
			result = restTemplate.getForObject("http://rproxy:8080/auth/trust?token={tokenValue}", UserDTO.class, params);
			return result;
		}catch(Exception e ){
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

        }
	}
	
	
}
