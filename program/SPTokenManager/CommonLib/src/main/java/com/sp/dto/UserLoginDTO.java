package com.sp.dto;

public class UserLoginDTO {
	
	public String login;
	public String password;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public UserLoginDTO() {
		super();
	}
	
	
	
}
