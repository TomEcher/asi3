package com.sp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
    private Long id;
	
	@Column(name = "pseudo", unique=true, insertable=true, updatable=true, nullable=false)
    public String pseudo;
	
	@Column(name = "password", insertable=true, updatable=true, nullable=false)
	public String password;
	
	@Column(name = "lname", insertable=true, updatable=true, nullable=false)
	public String lname;
	
	@Column(name = "fname", insertable=true, updatable=true, nullable=false)
	public String fname;
	
	@Column(name = "balance", insertable=true, updatable=true, nullable=false)
	public Float balance;
	

	public User(){}
	public User(String pseudo, String password,String lname, String fname, Float balance) {
		this.pseudo = pseudo;
		this.password = password;
		this.lname =lname;
		this.fname = fname;
		this.balance = balance;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id= id;
	}
	
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo=pseudo;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password=password;
	}
	
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname=lname;
	}
	
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname=fname;
	}
	
	public Float getBalance() {
		return balance;
	}
	public void setBalance(Float balance) {
		this.balance=balance;
	}
	
}
