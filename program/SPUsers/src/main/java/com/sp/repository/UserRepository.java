package com.sp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.sp.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	public List<User> findAll();
	public Optional<User> findById(Long id);
	public Optional<User> findByPseudo(String pseudo);
	public Optional<User> findByPseudoAndPassword(String pseudo, String password);
}
