package com.sp.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sp.dto.UserDTO;
import com.sp.dto.UserGetDTO;
import com.sp.dto.UserLoginDTO;
import com.sp.dto.UserPostDTO;
import com.sp.dto.UserPutDTO;
import com.sp.model.User;
import com.sp.service.UserService;

@Component
public class UserMapper {

	@Autowired
    UserService uService;
	
	public UserGetDTO getUserDTO(User user){

		return new UserGetDTO(user.getId(), user.getLname(),user.getFname(),user.getPseudo(), user.getBalance());
		
	}	
	
	public UserDTO toDTO(User user){

		return new UserDTO(user.getId(),user.getLname(),user.getFname(),user.getPseudo(), user.getBalance());
		
	}
	
	public User PutToModel(UserPutDTO userdto){
		User user = uService.getUser(userdto.getId_user());
		user.setBalance(user.getBalance() + userdto.getBalance());
		
		return user;
		
		
	}
	
	public User PosttoModel(UserPostDTO user){

		return new User(user.getPseudo(),user.getPassword(),user.getLname(), user.getFname(), (float) 5000);
		
	}
	
	public UserLoginDTO ToUserLoginDTO(User user) {
		UserLoginDTO userLogin = new UserLoginDTO();
		userLogin.setLogin(user.getPseudo());
		userLogin.setPassword(user.getPassword());
		return userLogin;
	}
}
