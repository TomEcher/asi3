package com.sp.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.sp.dto.MessageResponseDTO;
import com.sp.dto.UserDTO;
import com.sp.dto.UserGetDTO;
import com.sp.dto.UserLoginDTO;
import com.sp.dto.UserPostDTO;
import com.sp.dto.UserPutDTO;
import com.sp.mapper.UserMapper;
import com.sp.model.User;
import com.sp.repository.UserRepository;
import com.sp.service.UserService;
import com.sp.tools.UserLoginTools;

@RestController
@CrossOrigin(origins = "*")
public class UsersController {
	@Autowired
    UserService uService;
	
	@Autowired
    UserRepository uRepository;

    @Autowired
    UserMapper uMapper;
    
    @Autowired
    UserLoginTools ulTools;
    
    @Autowired
    HttpServletRequest request;
    
	/*
	 * WEB EndPoint
	 */
	@RequestMapping(method=RequestMethod.GET,value="/users")
	public ResponseEntity<UserGetDTO> getUser(){
		UserDTO user = ulTools.getUserByToken(request.getHeader("Authorization"));

		Optional<User> u = uService.getUserByPseudo(user.getPseudo());
		if(u.isPresent()) {
			return new ResponseEntity<UserGetDTO>(uMapper.getUserDTO(u.get()), HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/users")
	public ResponseEntity<MessageResponseDTO> addUser(@RequestBody UserPostDTO user){
		User userModel = null;
		try {
			userModel = uMapper.PosttoModel(user);
			uService.addUser(userModel, uMapper.ToUserLoginDTO(userModel));
			uService.Give5Cards(uMapper.toDTO(userModel));
		}catch(Exception e ){
            System.out.println("Duplication d'utilisateur");
    		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "user creation failed"), HttpStatus.CONFLICT);

        }
		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, "successful user creation"), HttpStatus.ACCEPTED);
	}
	
	/*
	 * S2S EndPoint
	 */
	@RequestMapping(method=RequestMethod.GET,value="/users/{pseudo}")
	public ResponseEntity<UserDTO> getUserByPseudo(@PathVariable String pseudo){
		User u=uService.getUser(pseudo);
        return new ResponseEntity<UserDTO>(uMapper.toDTO(u), HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(method = RequestMethod.POST,value = "/users/login")
	public ResponseEntity<MessageResponseDTO> findByPseudoAndPassword(@RequestBody UserLoginDTO user){
		try {
			if(uService.findByPseudoAndPassword(user.getLogin(), user.getPassword())==true) {
	            System.out.println("match");
	    		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, "match login"), HttpStatus.ACCEPTED);
			}
			else {
	            System.out.println("unmatch");
	    		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "unmatch login"), HttpStatus.CONFLICT);
			}
			
		}catch(Exception e ){
            System.out.println("Identifiants erron�s");
    		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 0, "unmatch login"), HttpStatus.CONFLICT);

        }
	}
			

	@RequestMapping(method=RequestMethod.PUT,value="/users/")
	public ResponseEntity<MessageResponseDTO> editUserByID(@RequestBody UserPutDTO user){
		User monUser = uMapper.PutToModel(user);
		uRepository.save(monUser);
		return new ResponseEntity<MessageResponseDTO>(new MessageResponseDTO((long) 1, "successful change balance"), HttpStatus.ACCEPTED);
	}
	
	
	
}
