package com.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;

import com.sp.dto.MessageResponseDTO;
import com.sp.dto.UserDTO;
import com.sp.dto.UserLoginDTO;
import com.sp.model.User;
import com.sp.repository.UserRepository;

@Service
public class UserService{
	@Autowired
	UserRepository uRepository;
		
    /**
    *
    * Cr�ation d'un utilisateur
    *
    */	
	public void addUser(User u, UserLoginDTO userLogin) {
		RestTemplate restTemplate = new RestTemplate();
        HttpEntity<UserLoginDTO> request = new HttpEntity<UserLoginDTO>(userLogin);
        UserLoginDTO result;
        try {
            result = restTemplate.postForObject("http://rproxy:8080/auth/crypt", request, UserLoginDTO.class);
            u.setPassword(result.getPassword());
    		User createdUser= uRepository.save(u);
    		System.out.println(createdUser);
        }catch(Exception e ){
            System.out.println("Duplication d'utilisateur");
        }
		
	}
	


	public void Give5Cards(UserDTO user) {
		RestTemplate restTemplate = new RestTemplate();
		try {
		    HttpEntity<UserDTO> entity = new HttpEntity<UserDTO>(user);
		    restTemplate.exchange("http://rproxy:8080/cards/give", HttpMethod.POST, entity, UserDTO.class);
		}catch(Exception e ){

			System.out.println("UPDATE ERROR");
        }
	}
    /**
    *
    * R�cup�ration des informations utilisateurs � partir de son ID
    *
    */	
	public User getUser(String pseudo) {
		Optional<User> uOpt =uRepository.findByPseudo(pseudo);
		if (uOpt.isPresent()) {
			return uOpt.get();
		}else {
			return null;
		}
	}
	
	
    /**
    *
    * R�cup�ration des informations utilisateurs � partir de son ID
    *
    */	
	public User getUser(Long id) {
		Optional<User> uOpt =uRepository.findById(id);
		if (uOpt.isPresent()) {
			return uOpt.get();
		}else {
			return null;
		}
	}
	
    /**
    *
    * R�cup�ration des informations utilisateurs � partir de son Pseudo
    *
    */	
	public Optional<User> getUserByPseudo(String pseudo){
		Optional<User> user = uRepository.findByPseudo(pseudo);
		return user;
	}

	
    /**
    *
    * R�cup�ration de l'ensemble des utilisateurs
    *
    */	
	public List<User> getAllUser() {
		List<User> user = uRepository.findAll();
		return user;
	}
	
	/**
    *
    * R�cup�ration des informations utilisateurs pour v�rifier sa connexion
    *
    */	
	public boolean findByPseudoAndPassword(String pseudo, String password){
		Optional<User> user= uRepository.findByPseudoAndPassword(pseudo,password);
		if(user.isPresent()) {
			return true;
		}
		return false;
	}

}
