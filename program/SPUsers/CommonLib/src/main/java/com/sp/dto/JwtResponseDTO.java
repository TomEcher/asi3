package com.sp.dto;

import java.io.Serializable;

public class JwtResponseDTO implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private final String jwttoken;

	public JwtResponseDTO(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public JwtResponseDTO() {
		this.jwttoken = "";
	}

	public String getToken() {
		return this.jwttoken;
	}
}