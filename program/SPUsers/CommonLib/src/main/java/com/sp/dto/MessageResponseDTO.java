package com.sp.dto;


public class MessageResponseDTO {

	public Long status;
	public String message;

	public MessageResponseDTO() {
	}	
	
	
	public MessageResponseDTO(Long status, String message) {
		super();
		this.status = status;
		this.message = message;
	}


	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	
}
