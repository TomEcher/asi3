Les membres du groupes :
	- Tom ECHER
	- Lucas THOREL

Les éléments réalisés du cahier des charges :
	- Conversion en Micro Service :
		- Users : Lucas & Tom
		- Cards/TemplateCards : Tom
		- Transactions : Lucas & Tom
		- TokenManager : Tom
	- Ajout du Micro Service Combat : Tom
	- Configuration Nginx : Lucas
	- FrontEnd : Lucas
	- Docker : Lucas & Tom (Pour lancer le programme, se placer dans './program' puis utiliser 'docker-compose up')
				Chaque MicroService est dans un container docker
		
Les éléments non-réalisés du cahier des charges :
	- Test Unitaire & Fonctionnel & SONAR
		Nous avons décidé de nous focaliser sur la mise en place des micro services, comme nous étions deux c'était compliqué de tout finir...

Lien dépôt GitLab :
	- https://gitlab.com/lucasthorel_cpe/asi2

Lien Youtube : 
	- https://youtu.be/dUY_uX2FPm0
